package com.FinancialManagment.ms_expense.aspect;

import com.FinancialManagment.ms_expense.dto.request.ExpenseRequest;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Aspect
@Component
public class RequestValidationAspect {

    @Pointcut("execution(* com.FinancialManagment.ms_expense.controller.*.*(..)) && " +
            "(args(@org.springframework.web.bind.annotation.RequestBody (*), ..) || " +
            "args(.., @org.springframework.web.bind.annotation.RequestBody (*)))")
    private void controllerMethods() {}



    @Before("controllerMethods() && @annotation(org.springframework.web.bind.annotation.PostMapping)")
    public void validatePostRequests(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        for (Object arg : args) {
            if (arg instanceof ExpenseRequest) {
                validateExpenseRequest((ExpenseRequest) arg);
                break;
            }
        }
    }

    @Before("controllerMethods() && @annotation(org.springframework.web.bind.annotation.PutMapping)")
    public void validatePutRequests(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        for (Object arg : args) {
            if (arg instanceof ExpenseRequest) {
                validateExpenseRequest((ExpenseRequest) arg);
                break;
            }
        }
    }


    private void validateExpenseRequest(ExpenseRequest expenseRequest) {
        if (expenseRequest.getTitle() == null || expenseRequest.getTitle().isEmpty()) {
            throw new IllegalArgumentException("Title must not be empty");
        }
        if (expenseRequest.getTitle().length() > 100) {
            throw new IllegalArgumentException("The length of the title cannot be more than 100 characters.");
        }

        if (expenseRequest.getNote() != null && expenseRequest.getNote().length() > 500) {
            throw new IllegalArgumentException("The length of the notes cannot be more than 500 characters.");
        }

        if (expenseRequest.getAmount() == null || expenseRequest.getAmount().compareTo(BigDecimal.ZERO) < 0) {
            throw new IllegalArgumentException("Amount must be greater than or equal to zero");
        }

        if (expenseRequest.getCurrency() == null || expenseRequest.getCurrency().isEmpty()) {
            throw new IllegalArgumentException("Currency must not be null or empty");
        }

        if (expenseRequest.getCategoryName() == null || expenseRequest.getCategoryName().isEmpty()) {
            throw new IllegalArgumentException("Category name must not be empty");
        }
    }
}
