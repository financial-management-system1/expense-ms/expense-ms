package com.FinancialManagment.ms_expense.dto.responce;

import com.fasterxml.jackson.annotation.JsonIgnore;

public record ExchangeRateDTO(String currencyCode, @JsonIgnore Double exchangeValue, Double convertedAmount) {
}
