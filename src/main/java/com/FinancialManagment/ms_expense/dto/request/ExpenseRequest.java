package com.FinancialManagment.ms_expense.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import jakarta.validation.constraints.*;


import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExpenseRequest {
    @NotBlank(message = "Title must not be blank")
    @Size(max = 100, message = "The length of the title cannot be more than 100 characters")
    private String title;
    @Size(max = 500, message = "The length of the note cannot be more than 500 characters")
    private String note;
    @NotNull(message = "Amount must not be null")
    @DecimalMin(value = "0.0", inclusive = false, message = "Amount must be greater than zero")
    private BigDecimal amount;
    @NotBlank(message = "Currency must not be blank")
    private String currency;
    private LocalDate expenseDate;
    @NotBlank(message = "Category name must not be blank")
    private String categoryName;
}
