package com.FinancialManagment.ms_expense.service.impl;

import com.FinancialManagment.ms_expense.client.ExchangeClient;
import com.FinancialManagment.ms_expense.client.UserClient;
import com.FinancialManagment.ms_expense.dto.request.BalanceRequest;
import com.FinancialManagment.ms_expense.dto.request.ExchangeRateRequest;
import com.FinancialManagment.ms_expense.dto.request.ExpenseRequest;
import com.FinancialManagment.ms_expense.dto.responce.AccountResponse;
import com.FinancialManagment.ms_expense.dto.responce.ExchangeRateDTO;
import com.FinancialManagment.ms_expense.dto.responce.ExpenseResponse;
import com.FinancialManagment.ms_expense.dto.responce.MonthlyExpenseResponse;
import com.FinancialManagment.ms_expense.enums.Currency;
import com.FinancialManagment.ms_expense.exception.ResourceNotFoundException;
import com.FinancialManagment.ms_expense.model.Category;
import com.FinancialManagment.ms_expense.model.Expense;
import com.FinancialManagment.ms_expense.model.MonthlyExpense;
import com.FinancialManagment.ms_expense.repository.CategoryRepository;
import com.FinancialManagment.ms_expense.repository.ExpenseRepository;
import com.FinancialManagment.ms_expense.repository.MonthlyExpenseRepository;
import com.FinancialManagment.ms_expense.service.ExpenseService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ExpenseServiceImpl implements ExpenseService {
    private final ExpenseRepository expenseRepository;
    private final CategoryRepository categoryRepository;
    private final MonthlyExpenseRepository monthlyExpenseRepository;
    private final UserClient userClient;
    private final ExchangeClient exchangeClient;

    @Transactional
    public ResponseEntity<String> addExpense(UUID userId, ExpenseRequest request) {
        try {
            String limitCheckMessage = checkUserLimit(userId, request.getAmount());

            Expense expense = new Expense();
            mapExpenseRequestToExpense(request, expense);
            setExpenseToCategory(request, expense);
            handleMonthlyExpense(request, expense, userId);
            expense.setUserId(userId);
            updateBalanceForAddExpense(String.valueOf(userId), expense);
            expenseRepository.save(expense);

            if (limitCheckMessage != null) {
                return ResponseEntity.ok("Expense added successfully, but " + limitCheckMessage);
            } else {
                return ResponseEntity.ok("Expense added successfully.");
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Expense could not be added: " + e.getMessage());
        }
    }


    @Transactional(readOnly = true)
    public List<ExpenseResponse> getAllExpenseByUserId(UUID userId) {
        List<Expense> expenses = expenseRepository.findByUserId(userId);
        return mapToExpenseResponseList(expenses);
    }

    @Transactional(readOnly = true)
    public List<MonthlyExpenseResponse> getMonthlyExpenseByUserId(UUID userId) {
        List<MonthlyExpense> monthlyExpenses = monthlyExpenseRepository.findByUserId(userId);

        monthlyExpenses.removeIf(monthlyExpense -> monthlyExpense.getExpenses().isEmpty());

        monthlyExpenses.forEach(monthlyExpense -> {
            if (monthlyExpense.getExpenses().isEmpty()) {
                monthlyExpenseRepository.delete(monthlyExpense);
            }
        });

        return monthlyExpenses.stream()
                .map(monthlyExpense -> {
                    BigDecimal totalExpense = calculateTotalExpense(monthlyExpense);
                    totalExpense = totalExpense.setScale(2, RoundingMode.HALF_UP);
                    List<ExpenseResponse> expenseResponses = mapToExpenseResponseList(monthlyExpense.getExpenses());
                    String currency = "AZN";
                    return new MonthlyExpenseResponse(
                            monthlyExpense.getMonth(),
                            totalExpense,
                            currency,
                            expenseResponses
                    );
                })
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public List<ExpenseResponse> getExpenseByCategoryName(UUID userId, String categoryName) {
        List<Expense> expenses = expenseRepository.findByUserIdAndCategoryCategoryNameIgnoreCase(userId, categoryName);

        if (expenses.isEmpty()) {
            throw new ResourceNotFoundException("No expenses found for the category: " + categoryName);
        }

        return mapToExpenseResponseList(expenses);
    }

    @Transactional
    public ResponseEntity<String> updateExpense(UUID userId, ExpenseRequest expenseRequest, Long id) {
        try {
            Expense existingExpense = expenseRepository.findById(id)
                    .orElseThrow(() -> new ResourceNotFoundException("Expense not found"));

            if (!existingExpense.getUserId().equals(userId)) {
                return ResponseEntity.status(HttpStatus.FORBIDDEN)
                        .body("You are not authorized to update this expense.");
            }

            BigDecimal oldAmount = existingExpense.getAmount();
            BigDecimal newAmount = expenseRequest.getAmount();

            BigDecimal oldAmountInAZN = convertToAZN(oldAmount, existingExpense.getCurrency());
            BigDecimal newAmountInAZN = convertToAZN(newAmount, expenseRequest.getCurrency());

            String limitCheckMessage = checkUserLimit(userId, newAmountInAZN.subtract(oldAmountInAZN));

            mapExpenseRequestToExpense(expenseRequest, existingExpense);

            MonthlyExpense oldMonthlyExpense = existingExpense.getMonthlyExpense();
            oldMonthlyExpense.setTotalExpense(oldMonthlyExpense.getTotalExpense().subtract(oldAmount));
            oldMonthlyExpense.setTotalExpense(oldMonthlyExpense.getTotalExpense().add(newAmount));

            updateBalanceForUpdate(userId.toString(), oldAmountInAZN, newAmountInAZN);
            setExpenseToCategory(expenseRequest,  existingExpense);

            expenseRepository.save(existingExpense);

            handleMonthlyExpense(expenseRequest, existingExpense, existingExpense.getUserId());

            if (limitCheckMessage != null) {
                return ResponseEntity.ok("Expense updated successfully, but " + limitCheckMessage);
            }

            return ResponseEntity.ok("Expense updated successfully.");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Expense could not be updated: " + e.getMessage());
        }
    }

    @Transactional
    public ResponseEntity<String> deleteExpense(UUID userId, Long expenseId) {
        try {
            Expense expense = expenseRepository.findById(expenseId)
                    .orElseThrow(() -> new ResourceNotFoundException("Expense not found"));

            if (!expense.getUserId().equals(userId)) {
                return ResponseEntity.status(HttpStatus.FORBIDDEN)
                        .body("You are not authorized to delete this expense.");
            }

            updateBalanceForDeleteExpense(userId.toString(), expense.getAmount(), expense.getCurrency());

            MonthlyExpense oldMonthlyExpense = expense.getMonthlyExpense();
            oldMonthlyExpense.setTotalExpense(oldMonthlyExpense.getTotalExpense().subtract(expense.getAmount()));

            expenseRepository.delete(expense);

            return ResponseEntity.ok("Expense deleted successfully.");
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Expense could not be deleted: " + e.getMessage());
        }
    }



    private void handleMonthlyExpense(ExpenseRequest expenseRequest, Expense expense, UUID userId) {
        LocalDate newMonthStart = expenseRequest.getExpenseDate() != null ?
                expenseRequest.getExpenseDate().withDayOfMonth(1) : LocalDate.now().withDayOfMonth(1);
        MonthlyExpense monthlyExpense = monthlyExpenseRepository.findByUserIdAndMonth(userId, newMonthStart)
                .orElseGet(() -> {
                    MonthlyExpense newMonthlyExpense = new MonthlyExpense();
                    newMonthlyExpense.setMonth(newMonthStart);
                    newMonthlyExpense.setTotalExpense(BigDecimal.ZERO);
                    newMonthlyExpense.setUserId(userId);
                    return monthlyExpenseRepository.save(newMonthlyExpense);
                });

        monthlyExpense.addExpense(expense);
        monthlyExpense.setTotalExpense(monthlyExpense.getTotalExpense().add(expense.getAmount()));
    }

    private void mapExpenseRequestToExpense(ExpenseRequest expenseRequest, Expense expense) {

        expense.setTitle(expenseRequest.getTitle());
        expense.setNote(expenseRequest.getNote());
        expense.setAmount(expenseRequest.getAmount());
        expense.setExpenseDate(expenseRequest.getExpenseDate() != null ? expenseRequest.getExpenseDate() : LocalDate.now());

        try {
            Currency currency = Currency.fromString(expenseRequest.getCurrency());
            expense.setCurrency(currency.name());
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Invalid currency: " + expenseRequest.getCurrency());
        }

    }

    public List<ExpenseResponse> mapToExpenseResponseList(List<Expense> expenses) {
        return expenses.stream()
                .map(expense -> {
                    BigDecimal convertedAmount = convertToAZN(expense.getAmount(), expense.getCurrency());
                    String formattedAmount = formatAmount(convertedAmount);
                    String convertedCurrency = getConvertedCurrency(expense); // Assuming conversion always to AZN
                    return new ExpenseResponse(
                            expense.getId(),
                            expense.getTitle(),
                            expense.getNote(),
                            expense.getAmount(),
                            expense.getCurrency(),
                            expense.getExpenseDate(),
                            expense.getCategory().getCategoryName(),
                            formattedAmount,
                            convertedCurrency
                    );
                })
                .collect(Collectors.toList());
    }


    private void setExpenseToCategory(ExpenseRequest request, Expense expense) {
        Category category = categoryRepository.findCategoryByCategoryNameIgnoreCase(request.getCategoryName());
        if (category == null) {
            category = categoryRepository.findCategoryByCategoryNameIgnoreCase("Other");
        }
        expense.setCategory(category);
    }

    private String getConvertedCurrency(Expense expense) {
        if ("AZN".equals(expense.getCurrency())) {
            return "AZN";
        } else {
            ExchangeRateDTO response = exchangeClient.convertToAZN(createExchangeRateRequest(expense));
            return response.currencyCode();
        }
    }

    private ExchangeRateRequest createExchangeRateRequest(Expense expense) {
        return new ExchangeRateRequest(
                expense.getCurrency(),
                expense.getAmount().doubleValue(),
                "AZN"
        );
    }

    private BigDecimal calculateTotalExpense(MonthlyExpense monthlyExpense) {
        BigDecimal totalExpense = BigDecimal.ZERO;

        for (Expense expense : monthlyExpense.getExpenses()) {
            BigDecimal convertedAmount = convertToAZN(expense.getAmount(), expense.getCurrency());
            totalExpense = totalExpense.add(convertedAmount);
        }
        return totalExpense;
    }


    private BigDecimal convertToAZN(BigDecimal amount, String currency) {
        if ("AZN".equals(currency)) {
            return amount;
        }

        ExchangeRateRequest exchangeRateDTO = new ExchangeRateRequest(currency, amount.doubleValue(), "AZN");
        ExchangeRateDTO responseEntity = exchangeClient.convertToAZN(exchangeRateDTO);

        if (responseEntity != null && responseEntity.convertedAmount() != null) {
            return BigDecimal.valueOf(responseEntity.convertedAmount());
        }
        throw new RuntimeException("Currency could not be converted to AZN."
        );
    }


    private void updateBalanceForAddExpense(String userId, Expense expense) {
        ResponseEntity<AccountResponse> accountResponse = userClient.getBalanceAndLimitByUserId(userId);
        AccountResponse response = Objects.requireNonNull(accountResponse.getBody(), "AccountResponse body must not be null");

        BigDecimal expenseAmountInAZN = convertToAZN(expense.getAmount(), expense.getCurrency());
        BigDecimal newBalance = response.balance().subtract(expenseAmountInAZN);
        BalanceRequest updateBalanceRequest = new BalanceRequest(newBalance);

        try {
            userClient.updateBalance(userId, updateBalanceRequest);
        } catch (Exception e) {
            throw new RuntimeException("Failed to update user balance.", e);
        }
    }

    private void updateBalanceForUpdate(String userId, BigDecimal oldAmountInAZN, BigDecimal newAmountInAZN) {
        ResponseEntity<AccountResponse> accountResponse = userClient.getBalanceAndLimitByUserId(userId);
        AccountResponse response = Objects.requireNonNull(accountResponse.getBody(), "AccountResponse body must not be null");

        BigDecimal currentBalance = response.balance();
        BigDecimal adjustedBalance = currentBalance.add(oldAmountInAZN).subtract(newAmountInAZN);
        BalanceRequest updateBalanceRequest = new BalanceRequest(adjustedBalance);

        try {
            userClient.updateBalance(userId, updateBalanceRequest);
        } catch (Exception e) {
            throw new RuntimeException("Failed to update user balance.", e);
        }
    }

    private void updateBalanceForDeleteExpense(String userId, BigDecimal amount, String currency) {
        BigDecimal amountInAZN = convertToAZN(amount, currency);

        ResponseEntity<AccountResponse> accountResponse = userClient.getBalanceAndLimitByUserId(userId);
        AccountResponse response = Objects.requireNonNull(accountResponse.getBody(), "AccountResponse body must not be null");

        BigDecimal newBalance = response.balance().add(amountInAZN);
        BalanceRequest updateBalanceRequest = new BalanceRequest(newBalance);

        try {
            userClient.updateBalance(userId, updateBalanceRequest);
        } catch (Exception e) {
            throw new RuntimeException("Failed to update user balance.", e);
        }
    }


    private String checkUserLimit(UUID userId, BigDecimal expenseAmount) {
        ResponseEntity<AccountResponse> accountResponse = userClient.getBalanceAndLimitByUserId(String.valueOf(userId));
        AccountResponse responseBody = accountResponse.getBody();

        if (responseBody == null || responseBody.limit() == null) {
            return null;
        }

        BigDecimal userLimit = responseBody.limit();

        List<MonthlyExpenseResponse> monthlyExpenses = getMonthlyExpenseByUserId(userId);
        BigDecimal totalExpense = monthlyExpenses.stream()
                .map(MonthlyExpenseResponse::getTotalExpense)
                .reduce(BigDecimal.ZERO, BigDecimal::add)
                .add(expenseAmount);
        if (totalExpense.compareTo(userLimit) > 0) {
            return "You have exceeded your limit!";
        }
        return null;
    }
    private String formatAmount(BigDecimal amount) {
        DecimalFormat df = new DecimalFormat("#,##0.00");
        return df.format(amount);
    }
}
