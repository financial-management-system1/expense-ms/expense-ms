package com.FinancialManagment.ms_expense.controller;

import com.FinancialManagment.ms_expense.dto.request.ExpenseRequest;
import com.FinancialManagment.ms_expense.dto.responce.ExpenseResponse;
import com.FinancialManagment.ms_expense.dto.responce.MonthlyExpenseResponse;
import com.FinancialManagment.ms_expense.model.Expense;
import com.FinancialManagment.ms_expense.service.impl.ExpenseServiceImpl;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/expense")
@RequiredArgsConstructor
public class ExpenseController {

    private final ExpenseServiceImpl expenseService;

    @PostMapping
    public ResponseEntity<String> addExpense(@RequestHeader("X-USER-ID") @NotBlank String userId, @Valid @RequestBody ExpenseRequest request) {
        return expenseService.addExpense(UUID.fromString(userId), request);
    }

    @GetMapping
    public ResponseEntity<List<ExpenseResponse>> getAllExpensesByUserId(@RequestHeader("X-USER-ID") @NotBlank String userId ) {
        List<ExpenseResponse> expenses = expenseService.getAllExpenseByUserId(UUID.fromString(userId));
        return ResponseEntity.ok(expenses);
    }

    @GetMapping("/month")
    public ResponseEntity<List<MonthlyExpenseResponse>> getMonthlyExpenseByUserId(@RequestHeader("X-USER-ID") @NotBlank String userId){
        List<MonthlyExpenseResponse> monthlyExpenseResponses = expenseService.getMonthlyExpenseByUserId(UUID.fromString(userId));
        return ResponseEntity.ok(monthlyExpenseResponses);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<String> updateExpense(@RequestHeader("X-USER-ID") @NotBlank String userId, @Valid @RequestBody ExpenseRequest expenseRequest, @PathVariable Long id ){
        return expenseService.updateExpense(UUID.fromString(userId), expenseRequest, id);
    }

    @GetMapping("/category/{categoryName}")
    public ResponseEntity<List<ExpenseResponse>> getExpenseByCategoryName (@RequestHeader("X-USER-ID") @NotBlank String userId, @PathVariable String categoryName){
        List<ExpenseResponse> expenses = expenseService.getExpenseByCategoryName(UUID.fromString(userId),categoryName);
        return ResponseEntity.ok(expenses);
    }
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<String> deleteExpense(@RequestHeader("X-USER-ID") @NotBlank String userId,  @PathVariable Long id ){
        return expenseService.deleteExpense(UUID.fromString(userId), id);
    }
}
