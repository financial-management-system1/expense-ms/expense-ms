package com.FinancialManagment.ms_expense.client;

import com.FinancialManagment.ms_expense.dto.request.ExchangeRateRequest;
import com.FinancialManagment.ms_expense.dto.responce.ExchangeRateDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "exchange-ms", url = "http://localhost:8089/exchange")
public interface ExchangeClient {
    @PostMapping("/converted")
    ExchangeRateDTO convertToAZN(ExchangeRateRequest request);
}
