package com.FinancialManagment.ms_expense.service.impl;

import com.FinancialManagment.ms_expense.model.Category;
import com.FinancialManagment.ms_expense.repository.CategoryRepository;
import com.FinancialManagment.ms_expense.service.CategoryService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Service
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;

    @Transactional(readOnly = true)
    public List<String> getAllCategoryNames() {
        return categoryRepository.findAll()
                .stream()
                .map(Category::getCategoryName)
                .collect(Collectors.toList());
    }
}
