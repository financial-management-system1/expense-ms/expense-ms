package com.FinancialManagment.ms_expense.enums;

public enum Currency {
    USD, AZN, EUR, RUB;
    public static Currency fromString(String text) {
        for (Currency currency : Currency.values()) {
            if (currency.name().equalsIgnoreCase(text)) {
                return currency;
            }
        }
        throw new IllegalArgumentException("No currency with name " + text + " found");
    }
}
