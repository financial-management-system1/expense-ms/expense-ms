package com.FinancialManagment.ms_expense.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;

public record ExchangeRateRequest(String sourceCurrency, Double amount, String targetCurrency) {

}
