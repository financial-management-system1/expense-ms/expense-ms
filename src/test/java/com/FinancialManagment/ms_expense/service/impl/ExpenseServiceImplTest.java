package com.FinancialManagment.ms_expense.service.impl;

import com.FinancialManagment.ms_expense.dto.request.ExchangeRateRequest;
import com.FinancialManagment.ms_expense.dto.request.ExpenseRequest;
import com.FinancialManagment.ms_expense.dto.responce.AccountResponse;
import com.FinancialManagment.ms_expense.dto.responce.ExchangeRateDTO;
import com.FinancialManagment.ms_expense.dto.responce.ExpenseResponse;
import com.FinancialManagment.ms_expense.dto.responce.MonthlyExpenseResponse;
import com.FinancialManagment.ms_expense.exception.ResourceNotFoundException;
import com.FinancialManagment.ms_expense.model.Category;
import com.FinancialManagment.ms_expense.model.Expense;
import com.FinancialManagment.ms_expense.model.MonthlyExpense;
import com.FinancialManagment.ms_expense.repository.CategoryRepository;
import com.FinancialManagment.ms_expense.repository.ExpenseRepository;
import com.FinancialManagment.ms_expense.repository.MonthlyExpenseRepository;
import com.FinancialManagment.ms_expense.client.ExchangeClient;
import com.FinancialManagment.ms_expense.client.UserClient;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;


public class ExpenseServiceImplTest {
    @Mock
    private ExpenseRepository expenseRepository;

    @Mock
    private CategoryRepository categoryRepository;

    @Mock
    private MonthlyExpenseRepository monthlyExpenseRepository;

    @Mock
    private UserClient userClient;

    @Mock
    private ExchangeClient exchangeClient;

    @InjectMocks
    private ExpenseServiceImpl expenseService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getExpenseByCategoryName_InvalidCategory_ThrowsException() {
        UUID userId = UUID.randomUUID();
        String categoryName = "Invalid";

        when(expenseRepository.findByUserIdAndCategoryCategoryNameIgnoreCase(userId, categoryName)).thenReturn(Collections.emptyList());

        Exception exception = assertThrows(ResourceNotFoundException.class, () -> expenseService.getExpenseByCategoryName(userId, categoryName));

        assertEquals("No expenses found for the category: " + categoryName, exception.getMessage());
        verify(expenseRepository, times(1)).findByUserIdAndCategoryCategoryNameIgnoreCase(userId, categoryName);
    }



    @Test
    void updateExpense_UnauthorizedUser_ReturnsForbidden() {
        UUID userId = UUID.randomUUID();
        UUID anotherUserId = UUID.randomUUID();
        Long expenseId = 1L;
        ExpenseRequest request = new ExpenseRequest("Updated Title", "Updated Note", BigDecimal.valueOf(200.0), "EUR", LocalDate.now(), "Food");
        Expense existingExpense = new Expense();
        existingExpense.setId(expenseId);
        existingExpense.setUserId(anotherUserId);
        existingExpense.setAmount(BigDecimal.valueOf(100.0));
        existingExpense.setCurrency("USD");

        when(expenseRepository.findById(expenseId)).thenReturn(Optional.of(existingExpense));

        ResponseEntity<String> response = expenseService.updateExpense(userId, request, expenseId);

        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertEquals("You are not authorized to update this expense.", response.getBody());
        verify(expenseRepository, times(0)).save(any(Expense.class));
    }



    @Test
    void deleteExpense_UnauthorizedUser_ReturnsForbidden() {
        UUID userId = UUID.randomUUID();
        UUID anotherUserId = UUID.randomUUID();
        Long expenseId = 1L;
        Expense existingExpense = new Expense();
        existingExpense.setId(expenseId);
        existingExpense.setUserId(anotherUserId);
        existingExpense.setAmount(BigDecimal.valueOf(100.0));
        existingExpense.setCurrency("USD");

        when(expenseRepository.findById(expenseId)).thenReturn(Optional.of(existingExpense));

        ResponseEntity<String> response = expenseService.deleteExpense(userId, expenseId);

        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertEquals("You are not authorized to delete this expense.", response.getBody());
        verify(expenseRepository, times(0)).delete(any(Expense.class));
    }
}
