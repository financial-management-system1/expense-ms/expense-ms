# Expense Management System

## Overview
The Expense Management System is a backend service designed to help users track their expenses, categorize them, and manage their financial records. It operates as part of a microservices architecture, ensuring scalability and modularity.

### Components
- **API Gateway:** Manages service routes and authentication.
- **User Service:** Handles user management tasks such as registration and authentication.
- **Expense Service:** Manages expense tracking, categorization, and currency conversion.
- **Budget Service:** Manages budget creation, modification, and tracking.
- **Notification Service:** Handles email notifications related to financial activities.

### Features
- Add, edit, delete, and view expenses.
- Categorize expenses automatically or manually.
- Convert expense amounts to AZN (Azerbaijani Manat) using an external exchange service.
- Update user balances based on total expenses.
- Fetch expenses by category, date, and other criteria.
- Calculate total and monthly expenses in AZN.

### Getting Started
#### Prerequisites
- Java 17
- Gradle 7.x
- Docker (for running microservices)

#### Setup
1. **Clone the Repository:**
   ```bash
   git clone <repository_url>
   cd <repository_name>
## API Endpoints

### ExpenseController

#### Add Expense

```http
POST /expenses
Headers: X-USER-ID: {userId}
```
##### Edit Expense
```http
PUT /expenses/{expenseId}
Headers: X-USER-ID: {userId}
```
##### Delete Expense
```http
DELETE /expenses/{expenseId}
Headers: X-USER-ID: {userId}
```
##### Expense By UserID
```http
GET /expenses/{userId}
Headers: X-USER-ID: {userId}
```
##### Monthly Expense By UserId
```http
GET /expenses/{userId}
Headers: X-USER-ID: {userId}
```
##### GET Categories
```http
GET /categoires/
```