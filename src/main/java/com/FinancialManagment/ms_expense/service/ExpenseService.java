package com.FinancialManagment.ms_expense.service;

import com.FinancialManagment.ms_expense.dto.request.ExpenseRequest;
import com.FinancialManagment.ms_expense.dto.responce.ExpenseResponse;
import com.FinancialManagment.ms_expense.dto.responce.MonthlyExpenseResponse;
import com.FinancialManagment.ms_expense.model.Expense;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.UUID;

public interface ExpenseService {
    ResponseEntity<String> addExpense(UUID userId, ExpenseRequest request);
    List<ExpenseResponse> getAllExpenseByUserId(UUID userId);
    List<ExpenseResponse> getExpenseByCategoryName(UUID userId, String categoryName);
    List<MonthlyExpenseResponse> getMonthlyExpenseByUserId(UUID userId);
    ResponseEntity<String> updateExpense (UUID userId, ExpenseRequest expenseRequest, Long expenseId);
    ResponseEntity<String> deleteExpense (UUID userId, Long expenseId);

}
