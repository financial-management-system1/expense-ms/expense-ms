package com.FinancialManagment.ms_expense.client;

import com.FinancialManagment.ms_expense.dto.request.BalanceRequest;
import com.FinancialManagment.ms_expense.dto.responce.AccountResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;


@FeignClient(name = "user-service", url = "http://localhost:8081/api/user/account")
public interface UserClient {
    @GetMapping
     ResponseEntity<AccountResponse> getBalanceAndLimitByUserId(@RequestHeader("X-USER-ID") String userId);
    @PutMapping
    ResponseEntity<String> updateBalance(@RequestHeader("X-USER-ID") String userId, @RequestBody BalanceRequest balanceRequest);

}
