package com.FinancialManagment.ms_expense.dto.request;

import java.math.BigDecimal;

public record BalanceRequest(BigDecimal balance) {
}
