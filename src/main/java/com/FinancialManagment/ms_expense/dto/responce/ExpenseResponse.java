package com.FinancialManagment.ms_expense.dto.responce;

import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDate;


@Data
@NoArgsConstructor
@AllArgsConstructor

public class ExpenseResponse {
    private Long id;
    private String title;
    private String note;
    private BigDecimal amount;
    private String currency;
    private LocalDate expenseDate;
    private String categoryName;
    private String formattedAmount;
    private String convertedCurrency;

}


