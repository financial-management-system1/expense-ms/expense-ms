package com.FinancialManagment.ms_expense.repository;

import com.FinancialManagment.ms_expense.model.Expense;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
public interface ExpenseRepository extends JpaRepository<Expense, Long> {
    List<Expense> findByUserId(UUID userId);
    List<Expense> findByUserIdAndCategoryCategoryNameIgnoreCase(UUID userId, String categoryName);



}
