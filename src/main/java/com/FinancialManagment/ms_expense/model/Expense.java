package com.FinancialManagment.ms_expense.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Currency;
import java.util.Date;
import java.util.UUID;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Expense {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "title")
    private String title;
    @Column(name = "note")
    private String note;
    @Column(name = "amount")
    private BigDecimal amount;
    @Column(name = "expense_date")
    private LocalDate expenseDate;
    @Column(name = "currency")
    private String currency;
    @Column(name = "user_id", nullable = false)
    private UUID userId;
    @ManyToOne
    @JoinColumn(name = "category_id", nullable = false)
    @JsonBackReference
    private Category category;
    @ManyToOne
    @JoinColumn(name = "monthly_expense_id", nullable = false)
    @JsonBackReference
    private MonthlyExpense monthlyExpense;



    public void setUserId(UUID userId) {
        this.userId = userId;
    }
}
