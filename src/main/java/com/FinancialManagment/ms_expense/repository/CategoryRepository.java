package com.FinancialManagment.ms_expense.repository;

import com.FinancialManagment.ms_expense.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CategoryRepository extends JpaRepository<Category, Long> {
    Category findCategoryByCategoryNameIgnoreCase(String categoryName);
}
