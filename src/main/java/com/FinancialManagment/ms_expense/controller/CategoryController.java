package com.FinancialManagment.ms_expense.controller;

import com.FinancialManagment.ms_expense.model.Category;
import com.FinancialManagment.ms_expense.service.impl.CategoryServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/categories")
@RequiredArgsConstructor
public class CategoryController {

    private final CategoryServiceImpl categoryService;

    @GetMapping()
    public ResponseEntity<List<String>> getAllCategories() {
        List<String> categories = categoryService.getAllCategoryNames();
        return ResponseEntity.ok(categories);
    }
}
