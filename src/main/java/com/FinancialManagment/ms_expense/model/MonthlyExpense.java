package com.FinancialManagment.ms_expense.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MonthlyExpense {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "month")
    private LocalDate month;
    @Column(name = "total_expense")
    private BigDecimal totalExpense;
    @Column(name = "user_id")
    private UUID userId;
    @OneToMany(mappedBy = "monthlyExpense", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @JsonManagedReference
    private List<Expense> expenses = new ArrayList<>();

    public void addExpense(Expense expense) {
        this.expenses.add(expense);
        expense.setMonthlyExpense(this);
    }
}
