package com.FinancialManagment.ms_expense.dto.responce;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
public class MonthlyExpenseResponse {
    private LocalDate month;
    private BigDecimal totalExpense;
    private String currency;
    private List<ExpenseResponse> expenses;
}
