package com.FinancialManagment.ms_expense.service;

import com.FinancialManagment.ms_expense.model.Category;

import java.util.List;

public interface CategoryService {
    List<String> getAllCategoryNames();
}
