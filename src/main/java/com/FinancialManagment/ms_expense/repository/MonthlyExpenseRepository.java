package com.FinancialManagment.ms_expense.repository;

import com.FinancialManagment.ms_expense.model.MonthlyExpense;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface MonthlyExpenseRepository extends JpaRepository<MonthlyExpense, Long> {
    List<MonthlyExpense> findByUserId(UUID userId);
    Optional<MonthlyExpense> findByUserIdAndMonth(UUID userId, LocalDate monthStart);
}
